package com.example.laba3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.example.laba3.R




class MainActivity : AppCompatActivity() {
    lateinit var emailTextView : TextView
    lateinit var passwordTextView: TextView
    lateinit var submitButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        emailTextView = findViewById(R.id.editTextTextEmailAddress)
        passwordTextView = findViewById(R.id.editTextTextPassword)
        submitButton = findViewById(R.id.submitButton)
        emailTextView.text = "Avadakedavra"
        submitButton.setOnClickListener {
            val intent = Intent(this,RecyclerActivity::class.java )
            startActivity(intent)
        }
    }

}